# Getting started

This project is based on [this sample project](https://tomcat.apache.org/tomcat-7.0-doc/appdev/sample/). It was put into docker following instructions from [this video](https://www.youtube.com/watch?v=B84_umDg2kY)

If you have a problem building the tomcat container, try opening docker desktop and going to `settings > resources > filesharing`. Add this directory as one of the directories able to be mounted.

run the following command to start the environment
```
docker-compose up -d
``` 

